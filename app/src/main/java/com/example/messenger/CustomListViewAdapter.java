package com.example.messenger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomListViewAdapter extends BaseAdapter {
    ArrayList<User> arrayList;
    Context context;

    @Override
    public int getCount() {
        return arrayList.size();
    }

    public CustomListViewAdapter(ArrayList<User> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.item_lv1, parent, false);
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.name = convertView.findViewById(R.id.txt_name);

        viewHolder.online = convertView.findViewById(R.id.img_online);

        viewHolder.offline = convertView.findViewById(R.id.img_offline);
        viewHolder.name.setText(arrayList.get(position).getName());
        if (arrayList.get(position).getStatus().equals("online")) {
            viewHolder.online.setVisibility(View.VISIBLE);
            viewHolder.offline.setVisibility(View.GONE);
        } else {
            viewHolder.offline.setVisibility(View.VISIBLE);
            viewHolder.online.setVisibility(View.GONE);
        }
        return convertView;

    }

    class ViewHolder {
        TextView name;
        CircleImageView online, offline;
    }
}
