package com.example.messenger;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Main2Activity extends AppCompatActivity {
    private Button dangki;
    private EditText ten,pass,id;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        dangki = findViewById(R.id.btn_dk);
        ten = findViewById(R.id.edt_ten);
        pass = findViewById(R.id.edt_pass);
        id = findViewById(R.id.edt_id);


        dangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dangki();
            }
        });

    }



    private void dangki(){
        String email=ten.getText().toString();
        String password = pass.getText().toString();
        String mId = id.getText().toString();
        if(email.length()!=0&&password.length()!=0&&mId.length()!=0){
            User user = new User(email,password,mId,"offline","null");
            myRef.child(email).setValue(user);
            Intent intent = new Intent();
            intent.putExtra("name",email);
            intent.putExtra("pass",password);
            setResult(RESULT_OK,intent);
            finish();
        }

    }
}
