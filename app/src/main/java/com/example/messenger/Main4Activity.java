package com.example.messenger;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Main4Activity extends AppCompatActivity {
    ListView listView;
    ArrayList<User> arrayList= new ArrayList<User>();
    CustomListViewAdapter customListViewAdapter;
    Button dangxuat,profile;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference myRef = database.getReference();
    String mid;
    String muser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        listView = findViewById(R.id.listview);
        Intent intent = getIntent();
        mid = intent.getStringExtra("id");
        muser = intent.getStringExtra("name");
        dangxuat = findViewById(R.id.btn_dangxuat1);
        profile = findViewById(R.id.btn_profile);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(Main4Activity.this,ProfileActivity.class);
                intent1.putExtra("curuser",muser);
                startActivity(intent1);
            }
        });

        dangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Main4Activity.this);
                builder.setCancelable(false);
                builder.setTitle("Đăng xuất");
                builder.setMessage("Bạn muốn đăng xuất ?");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        myRef.child("Users").child(muser).child("status").setValue("offline");
                        Toast.makeText(Main4Activity.this, "Đăng xuất thành công", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Main4Activity.this, Main3Activity.class);
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });


        myRef.child("Users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                arrayList.clear();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    if(!user.getId().equals(mid))
                        arrayList.add(user);
                }
                customListViewAdapter = new CustomListViewAdapter(arrayList,Main4Activity.this);
                listView.setAdapter(customListViewAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1 = new Intent(Main4Activity.this,MainActivity.class);
                intent1.putExtra("sender",muser);
                intent1.putExtra("receiver",arrayList.get(position).getName());
                startActivityForResult(intent1,4);
            }
        });
    }

    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onResume() {
        super.onResume();
        myRef.child("Users").child(muser).child("status").setValue("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        myRef.child("Users").child(muser).child("status").setValue("offline");
    }

}
