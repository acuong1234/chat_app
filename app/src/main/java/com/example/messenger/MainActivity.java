package com.example.messenger;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rv1;
    private EditText edt1;
    private Button btn1, dangxuat;
    private ArrayList<Messenger> listmess;
    private TextView nguoinhan;

    DateFormat dateFormat, timeFormat;
    Date Now, Time;
    String sender;

    Fragment fragment = null;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference myRef = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv1 = findViewById(R.id.rv_1);
        edt1 = findViewById(R.id.edt_1);
        btn1 = findViewById(R.id.btn_send);
        nguoinhan = findViewById(R.id.tv_nguoinhan);
        dangxuat = findViewById(R.id.btn_dangxuat);


        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dateFormat.setLenient(false);

        timeFormat = new SimpleDateFormat("ddMMyyyyyhhmmss");
        timeFormat.setLenient(false);


        rv1.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        rv1.setLayoutManager(manager);

        listmess = new ArrayList<Messenger>();

        final Adapter adapter = new Adapter(listmess, getApplicationContext());
        rv1.setAdapter(adapter);

        Intent intent = getIntent();
        sender = intent.getStringExtra("sender");
        final String receiver = intent.getStringExtra("receiver");
        nguoinhan.setText(receiver);

        myRef.child("Users").child(sender).child("status").setValue("online");


        myRef.child("ListMess").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listmess.clear();
                for (DataSnapshot postSnapShot : dataSnapshot.getChildren()) {
                    Messenger mess = postSnapShot.getValue(Messenger.class);
                    if((mess.getName().equals(sender)||mess.getReceiver().equals(sender))&&
                            (mess.getReceiver().equals(receiver)||mess.getName().equals(receiver))){
                        listmess.add(mess);
                        adapter.notifyDataSetChanged();
                        rv1.smoothScrollToPosition(listmess.size() - 1);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String a = edt1.getText().toString();
                edt1.setText("");
                Now = new Date();
                String time = dateFormat.format(Now);
                Messenger messenger = new Messenger(sender, a, time,receiver);
                myRef.child("ListMess").push().setValue(messenger);
            }
        });

        dangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setCancelable(false);
                builder.setTitle("Đăng xuất");
                builder.setMessage("Bạn muốn đăng xuất ?");
                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        myRef.child("Users").child(sender).child("status").setValue("offline");
                        Toast.makeText(MainActivity.this, "Đăng xuất thành công", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                        startActivity(intent);
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        myRef.child("Users").child(sender).child("status").setValue("online");
    }

    @Override
    protected void onPause() {
        super.onPause();
        myRef.child("Users").child(sender).child("status").setValue("offline");
    }


//    private void showMenu(){
//        PopupMenu popupMenu = new PopupMenu(this,dangxuat);
//        getMenuInflater().inflate(R.menu.menu1,popupMenu.getMenu());
//        popupMenu.show();
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu1,menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.itemmenu1:
//                FirebaseAuth.getInstance().signOut();
//                Toast.makeText(this, "Đăng xuất thành công", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(MainActivity.this,Main3Activity.class);
//                startActivity(intent);
//                break;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
