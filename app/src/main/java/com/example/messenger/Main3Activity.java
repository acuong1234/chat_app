package com.example.messenger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main3Activity extends AppCompatActivity {
    private Button dangnhap,dangki;
    private EditText ten,pass;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        dangnhap = findViewById(R.id.btn_dangnhap);
        dangki = findViewById(R.id.btn_dangki);
        ten = findViewById(R.id.edt_ten);
        pass = findViewById(R.id.edt_pass);



        dangki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main3Activity.this,Main2Activity.class);
                startActivityForResult(intent,99);
            }
        });

        dangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dangnhap();
            }
        });
    }

    private void dangnhap(){
        final String email = ten.getText().toString();
        final String password = pass.getText().toString();
        myRef.child("Users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    if(email.equals(user.getName())&&password.equals(user.getPass())){
                        Toast.makeText(Main3Activity.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();

                        myRef.child("Users").child(user.getName()).child("status").setValue("online");

                        SharedPreferences sharedPreferences = getSharedPreferences("id",MODE_PRIVATE);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("currentuser",user.getName());
                        edit.commit();

                        Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
                        intent.putExtra("id",user.getId());
                        intent.putExtra("name",user.getName());
                        startActivityForResult(intent,3);
                    }
                }
                Toast.makeText(Main3Activity.this, "Đăng nhập thất bại", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==99&&resultCode==RESULT_OK){
            ten.setText(data.getStringExtra("name"));
            pass.setText(data.getStringExtra("pass"));
        }
    }

    @Override
    public void onBackPressed() {
    }
}
