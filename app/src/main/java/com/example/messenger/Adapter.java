package com.example.messenger;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    ArrayList<Messenger> list;
    Context context;

    public Adapter(ArrayList<Messenger> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (viewType) {
            case 1:
                View itemview = inflater.inflate(R.layout.item1, viewGroup, false);
                return new ViewHolder(itemview);
            case -1:
                View itemview1 = inflater.inflate(R.layout.item, viewGroup, false);
                return new ViewHolder(itemview1);
            default:
                return null;
        }

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.Mess.setText(list.get(i).getMess());
        viewHolder.Name.setText(list.get(i).getName());
        viewHolder.Time.setText(list.get(i).getTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("id", Context.MODE_PRIVATE);
        String currentid = sharedPreferences.getString("currentuser", null);
        if (list.get(position).getName().equals(currentid))
            return 1;
        else
            return -1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView Name, Mess, Time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            Name = itemView.findViewById(R.id.tv_name);
            Mess = itemView.findViewById(R.id.tv_mess);
            Time = itemView.findViewById(R.id.tv_time);
        }
    }
}




