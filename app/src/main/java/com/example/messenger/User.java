package com.example.messenger;

public class User {
    private String name;
    private String pass;
    private String id;
    private String status;
    private String avatarurl;

    public String getAvatarurl() {
        return avatarurl;
    }

    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl;
    }

    public User(String name, String pass, String id, String status, String avatarurl) {
        this.name = name;
        this.pass = pass;
        this.id = id;
        this.status = status;
        this.avatarurl = avatarurl;
    }

    public User(String name, String pass, String id, String status) {
        this.name = name;
        this.pass = pass;
        this.id = id;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User() {
    }

    public User(String name, String pass, String id) {
        this.name = name;
        this.pass = pass;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
