package com.example.messenger;

import android.widget.TextView;

import java.util.Date;

public class Messenger {
    private String sender, mess, time, receiver;


    public Messenger() {
    }

    public Messenger(String name, String mess, String time) {
        this.sender = name;
        this.mess = mess;
        this.time = time;
    }

    public Messenger(String name, String mess, String time, String receiver) {
        this.sender = name;
        this.mess = mess;
        this.time = time;
        this.receiver = receiver;
    }


    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return sender;
    }

    public void setName(String name) {
        this.sender = name;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
